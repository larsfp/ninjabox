#!/usr/bin/env python3

#Depends
# sudo apt install python3-pip
# sudo pip3 install --upgrade setuptools
# sudo pip3 install RPI.GPIO adafruit-blinka
# sudo apt-get install python3-pygame
# Enable i2c and SPI

verbose=False

import time
start_time = time.time()
if verbose: print("Importing modules")

import sys
import smbus # for the pcf8591 (LDR)
from pygame import mixer  # For alarm sound

import RPi.GPIO as GPIO # Laser
GPIO.setmode(GPIO.BCM)
#GPIO.setwarnings(False)

laser_pin = 26
ldr_difference = 50
# SPI pins for DotStar, 10 and 11
shake_pin = 16

# Colors for DotStar
red = (255, 0, 0)
yellow = (255, 255, 0)
green = (0, 255, 0)
black = (0, 0, 0)
white = (255, 255, 255)

#check your PCF8591 address by type in 'sudo i2cdetect -y -1' in terminal.
def setup_pcf8591(Addr):
	global address
	address = Addr

def read_pcf8591(chn): #channel
    if verbose:
        print("Reading from pcf at channel %s" %chn)
    try:
        if chn == 0:
            bus.write_byte(address,0x40)
        elif chn == 1:
            bus.write_byte(address,0x41)
        elif chn == 2:
            bus.write_byte(address,0x42)
        elif chn == 3:
            bus.write_byte(address,0x43)

        for _ in range (0, 1):
            _ = bus.read_byte(address) # dummy read to start conversion
    except Exception:
        print ("Address: %s" % address)

    return bus.read_byte(address)

#def write_pcf8591(val):
#	try:
#		temp = val # move string value to temp
#		temp = int(temp) # change string to integer
#		bus.write_byte_data(address, 0x40, temp)
#	except Exception:
#		print ("Error: Device address: 0x%2X" % address)

def play_dead(duration=5):
    # on accel detect, play broken, disable lasers, after some time, return to normal

    if verbose:
        print("Playing dead for %s" % duration)

    # Lasers off
    set_lasers(False)

    # Blink white, then turn lights off
    strip.fill(white); strip.show()
    sparks.play()
    time.sleep(0.2)
    strip.fill(black); strip.show()

    if not verbose:
        time.sleep(duration)

    # Slowly turn power back on
    powerup()

    # Lasers on
    set_lasers(True)

    if verbose:
        print("Alarm back on-line.")

def sound_alarm(duration=10):
    global strip
    # on LDR detect, play sound for some time, then return to normal

    if verbose:
        print("Alarm! LDRs detected anomaly!")
    strip.fill(red)
    strip.show()

    alarm.play()
    if verbose:
        print("Alarm done, returning to normal.")

    if not verbose:
        time.sleep(duration)

    powerup()

def powerup():
    global strip
    # Turn on power (yellow pixels), slowly
    delay = 0.4

    head  = 0  # Index of first 'on' pixel

    for i in range (0,3):
        strip[head] = green
        strip.show()
        time.sleep(delay)
        head += 1

    time.sleep(delay*2)
    power_up.play()

    strip.fill(yellow)
    strip.show()

    set_lasers(state=True)

def set_lasers(state=True):
    if verbose:
        print("Setting lasers %s" % state)
    GPIO.output(laser_pin, state)

def is_laser_hitting():
    read_delay = 0.01

    # turn laser off
    set_lasers(state=False)

    ## Check LDR
    time.sleep(read_delay)
    ldr_when_laser_off = read_pcf8591(0)
    if verbose:
        print("LDR is %s when laser is off" % (ldr_when_laser_off))

    ## turn lasers on
    set_lasers(state=True)

    ### Check LDR
    time.sleep(read_delay)
    ldr_when_laser_on = read_pcf8591(0)
    if verbose:
        print("LDR is %s when laser is on" % ldr_when_laser_on)

    if abs(ldr_when_laser_off - ldr_when_laser_on) > ldr_difference:
        return True
    else:
        return False

# main

if 'verbose' in sys.argv:
    verbose = True

# init shake sensor
GPIO.setup(shake_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# init sound
if verbose: print("* Importing alarm sound")
mixer.init()
alarm = mixer.Sound('alarm.wav')
sparks = mixer.Sound('sparks.wav')
power_up = mixer.Sound('power_up.wav')

# init lasers
if verbose: print("* Init laser")
GPIO.setup(laser_pin, GPIO.OUT)

# init leds
if verbose: print("* Init LEDs")
import adafruit_dotstar, board
numpixels = 4 # Number of LEDs in strip
strip = adafruit_dotstar.DotStar(board.SCK, board.MOSI, numpixels, brightness=0.2)
powerup()

# init LDRs
if verbose: print("* Init LDR")
# for RPI version 1, use "bus = smbus.SMBus(0)"
bus = smbus.SMBus(1)
setup_pcf8591(0x48)

# loop
if verbose: print("* Startup done")
delay=0.1

while True:
    try:
        shake_prev = None
        shake = None

        for i in range(0,10):
            shake_prev = shake
            shake = GPIO.input(shake_pin)
            #if verbose: print("Shake %s" % shake)
            if shake != shake_prev and shake != None and shake_prev != None:
                play_dead()
                shake = None
            time.sleep(0.01)
    
        # Check alarm
        if not is_laser_hitting():
            sound_alarm()
    
        time.sleep(delay)
    except KeyboardInterrupt:
        strip.fill(0)
        GPIO.cleanup()
        sys.exit()
