# Ninjabox

A tribute to https://en.wikipedia.org/wiki/Mark_of_the_Ninja

In the game you have to sneak into buildings with high tech alarms. Some of the laser traps can be deactivated by knocking out the fuse box with a dart.

So I made a laser alarm with a fusebox that can be knocked out.

## Features

* Glowing ⚡ with simple animations and color changes (start-up, alarm, sparks)
* Laser triggered alarm, which can't be fooled by using a separate laser or flash light.
* Motion activated "play dead"
* Play sounds for alarm, sparks, start-up

## Parts

* Raspberry Pi with power and SD card
* Speaker with built-in amp (for alarm and sound effects)
* DotStar (i.e. 4 LEDs) for a glowing "high voltage" logo on the fuse box
* Laser module, https://www.dx.com/p/arduino-650nm-laser-sensor-module-black-137473
* Photoresistor module, https://www.ebay.com/itm/PCF8591-AD-DA-converter-module-analog-to-digital-to-analog-conversion-Arduino/311566976754?hash=item488ad6c6f2:g:O1QAAOSwkl5XfxlW:rk:6:pf:0
* Tilt sensor, https://www.ebay.com/itm/1PCS-Tilt-Sensor-Vibration-Sensor-Module-for-Arduino-STM32-AVR-Raspberry-pi/381610633655?epid=1866867829&hash=item58d9c429b7:g:A2QAAOSwjKpXHIXd:rk:4:pf:0

## Setup
* Enable SPI for DotStar.
* Connect DotStar to 5V, GND, GPIO10 (MOSI) and GPIO11 (SCLK).
* Enable i2c for photoresistor module.
* Connect photoresistor module to 5V, GND, GPIO2 (i2c data) and GPIO3 (i2c clock).
* Connect laser module to 5V, GND and signal pin to i.e. GPIO26.
* Connect tilt sensor via a resistor to GPIO and i.e. GPIO16.
* Connect speaker with a jack cable.

## Test

In test/ you can find test programs for each sensor to make sure it's set up correctly.

## Media files

* I won't upload any sound files, because I don't know the license to those I have. Find your own, there are lots of free sound effects sites.
