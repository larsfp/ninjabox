#!/usr/bin/env python3

import time
import RPi.GPIO as GPIO # Laser
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

pin = 16
GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

while True:
    print(GPIO.input(pin))
    time.sleep(0.01)
