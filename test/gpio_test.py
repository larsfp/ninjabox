import sys
import RPi.GPIO as GPIO # Laser
GPIO.setmode(GPIO.BCM)

laser_pin = 26
GPIO.setup(laser_pin, GPIO.OUT)
GPIO.output(laser_pin, int(sys.argv[1]))

